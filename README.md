**This is a boiler plate of the salesforce testing via webdriver salesforce lightning component**

For this challenge I used webdriver.io 
I have setup a webdriver.io testscript based on the requested questions

Chronologically I picked the tasks up like this as it was easier for trial and error 
Task at hand which I did first: 

achievenements **automated scenarios** 

1. using a trailhead dev environment of salesforce https://curious-bear-869u8y-dev-ed.my.salesforce.com/ page:
2. logging in via google authenticor (setup in salesforce)
3. Create a contact in parrallel 2 times in chrome Iand in firefox"  via ci/cd (chrome only locally)
4. integration webhook between bitbucket and gitlab, publishing build job results to bitbucket
5. fetching shadowdom css via in the new salesforece lightning views 
5. integration jira, to define defects if a build failes directly from bitbucket
6. page object boilerplate

**Sync/Async**
 used the sync methology to build the **webdriver.io salesforce testscript** test script

 
Todo to learn for myself with wdio
1. Reporting of execution + video (-not done set up in my allure branch (for now using spec headless solution) 
2. reusing testsuite.xml output (allure report) store them S3/google cloud bucket and upload it to netlify as well as allure framework to fetch history (todo :seperate project, own extra challenge)
3. Integrate with **AWS device farm** **sauce labs** **browserstack** to run the script on firefox, chrome, and do android mobile testing in parrallel

