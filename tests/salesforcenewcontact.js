import {short, medium, long} from '../lib/timeouts'
import {mobile,tablet,desktop} from '../lib/devices'
import App from '../page-objects/App'
import SalesforceLogin from '../page-objects/pages/Salesforcelogin'





describe("Load Salesforce ", () => {
    it("Salesforce Demo", () => {
     
         //this testcase is using a simple pageobjects model in javascript, leveraging on the ShowroomPage and inheriting pause from the base class
        App.openSalesforceLogin()
        //fetch the list 
        browser.setWindowSize(tablet[0], tablet[1])
        SalesforceLogin.pauseLong()
     
        const username = $("[class$=username]")
        const password = $("[id$=password]")
        username.setValue("ralphvanderhorst@curious-bear-869u8y.com")
        password.setValue("London11!")
        SalesforceLogin.pauseShort()

        const login = $("[id$=Login]")
        login.waitForExist()
        login.click()
       
        
       


        //https://curious-bear-869u8y-dev-ed.lightning.force.com/
        //username ralphvanderhorst@curious-bear-869u8y.com
        //password
        //totp
       //CX4275P5LBEOSE3RZ4EWWS6VZCFZQQ5C

  
    })
    
    it("Will log in via google Authenticator", () => {
        const fillsecret = $("[id$=tc]")
        var totp = require('totp-generator');
        var token = totp('CX4275P5LBEOSE3RZ4EWWS6VZCFZQQ5C');
        fillsecret.setValue(token)
        const verify = $("[title$=Verify]")
        verify.click()
        SalesforceLogin.pauseLong()
        browser.url("https://curious-bear-869u8y-dev-ed.lightning.force.com/lightning/page/home");
        SalesforceLogin.pauseLong()
     
       // const isValid = totp.check(token, secret);
       // const isValid = totp.verify({ token, secret });
        
    })

    it("Click on New Contact", () => {
       

        const clickonContacts = $('//a[contains(@title,"Contacts")]')
        browser.execute("arguments[0].click();", clickonContacts)
        SalesforceLogin.pauseLong()
        const newcontact = $("//a[contains(@Title,'New')]")
        newcontact.click()
        SalesforceLogin.pauseLong()

     
       // const isValid = totp.check(token, secret);
       // const isValid = totp.verify({ token, secret });
     
    })

    it("Save the New Contact", () => {
         //click on salutation via shadow dom
         const selecttext = $("[class*=salutation]").shadow$("[class$=select")
         selecttext.click()
         SalesforceLogin.pauseLong()
         //select radio button
         const selectradiomr = $('[title$="Mrs."]')
         selectradiomr.click()
         SalesforceLogin.pauseLong()
         const lastname = $("//*[contains(@class,'lastName')]")
         lastname.waitForExist()
         lastname.setValue("Van der Horst")
         SalesforceLogin.pauseLong()
         const savecontact = $("//*[@title='Save']") 
         savecontact.click()
        
    })


  


 
});


