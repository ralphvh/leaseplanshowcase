import Base from '../Base'

class ShowroomPage extends Base{
    //object definitions
    get cookie() {
        return $("[class$='optanon-allow-all accept-cookies-button']")
    }
    get makeModel() {
    return $("//div[contains(@data-component,'desktop-filters')]/descendant::h3[contains(@data-key,'Make')]/ancestor::button/div")
    }


    //functions
    CookieIsVisible() {
        this.cookie.waitForExist()
    }
    ClickonAcceptCookie() {
        this.cookie.click()
    }
}

export default new ShowroomPage()